FROM ubuntu:18.04
MAINTAINER sharky@sharky.pw

RUN mkdir /app

# https://sia.tech/static/releases/sia.pem
ADD sia.tech.pem /app/sia.tech.pem

RUN apt update
RUN apt install wget unzip -y
RUN cd /app \
	&& wget https://sia.tech/static/releases/Sia-v1.3.7-linux-amd64.zip \
	&& unzip Sia-v1.3.7-linux-amd64.zip \
	&& mv Sia-v1.3.7-linux-amd64/* . \
	&& openssl dgst -sha256 -verify sia.tech.pem -signature siad.sig siad || false
	
VOLUME ["/data"]
	
ENTRYPOINT ["/app/siad", "-d", "/data"]
